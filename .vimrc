syntax on
let mapleader=' '

" :help <setting>
set backspace=indent,eol,start
set clipboard=unnamedplus
set encoding=utf-8
set hidden
set nocompatible
set noswapfile
set number relativenumber
set splitbelow
set splitright

nnoremap <c-h> <c-w>h
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-l> <c-w>l

nnoremap <leader>c :!%s//gI<left><left><left>
nnoremap <leader>d :Vex
nnoremap <leader>e :vsplit ~/.vimrc<cr>
nnoremap <leader>m :!mv<space>%<space>
nnoremap <leader>q :q<cr>
nnoremap <leader>s :source ~/.vimrc<cr>
nnoremap <leader>t :!touch<space>
nnoremap <leader>w :w<cr>
