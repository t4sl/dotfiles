"                        _
"  _ __   ___  _____   _(_)_ __ ___
" | '_ \ / _ \/ _ \ \ / / | '_ ` _ \
" | | | |  __/ (_) \ V /| | | | | | |
" |_| |_|\___|\___/ \_/ |_|_| |_| |_|

" Leader Key

let mapleader=' '

"
" VIM-PLUG
"
call plug#begin('~/.local/share/nvim/plugged')

" Colorscheme
Plug 'fenetikm/falcon'

" Status bar
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Git
Plug 'airblade/vim-gitgutter'

" Colors
Plug 'norcalli/nvim-colorizer.lua'

" Focus on writing
Plug 'junegunn/goyo.vim'

" Utilities
Plug 'terryma/vim-multiple-cursors'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

" LaTeX
Plug 'lervag/vimtex'

" NERDTree
Plug 'preservim/nerdtree'

call plug#end()

" ------

colorscheme falcon
let g:falcon_airline=1
let g:airline_theme='falcon'
let g:airline_powerline_fonts = 1
let g:airline#extensions#wordcount#enabled = 1
let g:airline#extensions#hunks#non_zero_only = 1

" Git Gutter
" highlight GitGutterAdd guifg=#009900 ctermfg=Green
" highlight GitGutterChange guifg=#bbbb00 ctermfg=Yellow
" highlight GitGutterDelete guifg=#ff2222 ctermfg=Red
nmap ) <Plug>(GitGutterNextHunk)
nmap ( <Plug>(GitGutterPrevHunk)
let g:gitgutter_enabled = 1
let g:gigutter_map_keys = 0

" Vimtex
let g:tex_flavor = 'latex'
let g:vimtex_view_general_viewer = 'zathura'

" deoplete
let g:deoplete#enable_at_startup = 1
call deoplete#custom#var('omni', 'input_patterns', { 'tex': g:vimtex#re#deoplete })

" NERDTree
map <C-n> :NERDTreeToggle<CR>
" Close if only window opened
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif



filetype plugin indent on
" :help <setting>
syntax on
set encoding=utf-8
set expandtab
set shiftwidth=4
set softtabstop=4
set tabstop=4
set noswapfile
set number relativenumber
set termguicolors
set updatetime=100

set clipboard+=unnamedplus

" Auto completion
set wildmode=longest,list,full

" Fix splitting
set splitbelow splitright

" ---

" Vertically center document when entering insert mode
autocmd InsertEnter * norm zz

" Remove trailing whitespace on save
autocmd BufWritePre * %s/\s\+$//e

" Enable and disable auto comment
map <leader>c :setlocal formatoptions-=cro<cr>
map <leader>C :setlocal formatoptions=cro<cr>

" Enable spell checking
map <leader>s :setlocal spell! spelllang=en_us<cr>

" Enable and disable auto indent
map <leader>i :setlocal autoindent<cr>
map <leader>I :setlocal noautoindent<cr>

" Enable/Disable Goyo
map <leader>g :Goyo<CR>


" Cursor line
set cursorline
"set cursorcolumn
"highlight CursorLine ctermbg=Yellow cterm=bold guibg=#2b2b2b
"highlight CursorColumn ctermbg=Yellow cterm=bold guibg=#2b2b2b
