# Arch Linux

- [archlinux.org](https://archlinux.org/)
- [wiki.archlinux.org](https://wiki.archlinux.org/)

Aqui está o passo a passo para inicializar um repositório git similar.

```sh
# Configura o git
git config --global user.name "Thiago Lopes"
git config --global user.email "t4sl@pm.me"
# Cria um diretório
mkdir ~/.files
# Inicializa um repositório nele
git init --bare $HOME/.files
# Define um atalho para o comando
alias d='/usr/bin/git --git-dir=$HOME/.files/ --work-tree=$HOME'
# Configura para ignorar arquivos
d config status.showUntrackedFiles no
```

Após criar um projeto no [GitLab](https://gitlab.com/t4sl), adiciona-se uma configuração adicional no repositório criado, e um arquivo utilitário para preservar as credenciais, que naõ deve ser marcado.

```sh
# Configura o servidor do GitLab para o repositório local
d remote add origin https://gitlab.com/t4sl/dotfiles.git
```

Cria-se o arquivo `~/.netrc`:

```
machine gitlab.com
login $username
password $password
```

Por enquanto, é só.


### Installation

The installation of Arch Linux is composed of two steps. The first happens inside of the installation media, and the second is the configuration of the system created.

The starting point is configure the keyboard. By default, the _Num Lock_ key is disabled.

```sh
# Search for existing keymaps
localectl list-keymaps | grep br

# Load the keyboard layout
loadkeys br-abnt
```

To set up the Internet via cable or WiFi:

```sh
# List available interfaces
ip link

# Check if it's working
ping -c 3 archlinux.org

# Sync local time with the Internet
timedatectl set-ntp true

# Define the timezone
timedatectl set-timezone America/Sao_Paulo

# Connect to a router
iwctl
## station wlan0 scan
## station wlan0 get-networks
## station wlan0 connect $name

# Using SSH to install remotely
# Discover the IP address
ip a

# Define a password for the root
passwd

# Start the SSH daemon
systemctl start sshd

# From the client side, invoke:
ssh root@192.168.0.?
```

Ok, now disk partitioning, make sure to backup everything you need or data will be lost.

```sh
# Invoke the program on the hard drive
fdisk /dev/sda
# g
# n . . +1.512G
# t 1
# n . . .
#
```

## Pacman

base base-devel linux linux-firmware linux-headers

intel-ucode xf86-video-intel

mate mate-extra network-manager-applet

xdg-user-dirs xdg-utils lightdm lightdm-gtk-greeter

blueman pulseaudio-bluetooth

tree bash-completion vim tmux alacritty neovim

ttf-liberation ttf-dejavu ttf-roboto ttf-roboto-mono

keepassxc xclip

vlc ffmpeg youtube-dl redshift transmission-gtk

libreoffice zathura zathura-pdf-poppler zotero texlive-most

python-pip

steam discord

firewalld docker

hplip cups inkscape inkscape-label-guides

postgresql tt-rss php-pgsql

## Yay

brave-bin
