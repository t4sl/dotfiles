#        _ _                     
#   __ _| (_) __ _ ___  ___  ___ 
#  / _` | | |/ _` / __|/ _ \/ __|
# | (_| | | | (_| \__ \  __/\__ \
#  \__,_|_|_|\__,_|___/\___||___/

alias d='/usr/bin/git --git-dir=$HOME/.files/ --work-tree=$HOME'
alias m="mkdir -pv"
alias la="ls -alhN --color=auto --group-directories-first"
alias ls="ls -lhN --color=auto --group-directories-first"
alias p="sudo pacman"
alias s="sudo systemctl"
alias t="tree -L 2"
alias y="yay"
alias ya="youtube-dl --add-metadata -xic"
alias yv="youtube-dl --add-metadata -ic"
