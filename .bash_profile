#                   __ _ _
#  _ __  _ __ ___  / _(_) | ___
# | '_ \| '__/ _ \| |_| | |/ _ \
# | |_) | | | (_) |  _| | |  __/
# | .__/|_|  \___/|_| |_|_|\___|
# |_|

# Set global variables
export BROWSER="brave"
export EDITOR="vim"

# Source .bashrc if exists
[ -f ~/.bashrc ] && source ~/.bashrc
