#  _               _
# | |__   __ _ ___| |__  _ __ ___
# | '_ \ / _` / __| '_ \| '__/ __|
# | |_) | (_| \__ \ | | | | | (__
# |_.__/ \__,_|___/_| |_|_|  \___|

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Input console format
PS1='[\u@\h \W]\$ '

# Disable ctrl-s and ctrl-q
stty -ixon

# Allows to change directory by typing the name
shopt -s autocd

# Infinite history
HISTSIZE= HISTFILESIZE=

# Import the aliases
[ -f ~/.bash_aliases ] && source ~/.bash_aliases
